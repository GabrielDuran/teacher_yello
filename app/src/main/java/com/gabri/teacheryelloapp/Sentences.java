package com.gabri.teacheryelloapp;

import java.io.Serializable;

// Class Sentences
class Sentences implements Serializable{

    // Attributes
    int button;
    String sentence, result;

    // Constructor method without parameters
    public Sentences(){super();}

    // Constructor method with parameters
    Sentences(int button, String sentence, String result){
        this.button = button;
        this.sentence = sentence;
        this.result = result;
    }

    // Get button icon
    public int getButton() { return button; }

    // Set button icon
    public void setButton(int button) {
        this.button = button;
    }

    // Get string sentence
    public String getSentence() { return sentence; }

    // Set string sentence
    public void setSentence(String sentence) { this.sentence = sentence; }

    // Get string result
    public String getResult() {
        return result;
    }

    // Set string result
    void setResult(String result) {
        this.result = result;
    }
}

package com.gabri.teacheryelloapp;

import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.Locale;

public class FragmentExplanation extends Fragment implements View.OnClickListener {

    // Attributes
    private TextToSpeech mTextToSpeech;
    private int mResult;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_explanation, container, false);

        // Matches parameters with U/I elements
        ImageView mMouthImage = (ImageView) rootView.findViewById(R.id.mouthPosition);
        TextView mLips = (TextView) rootView.findViewById(R.id.lips_description);
        TextView mJaw = (TextView) rootView.findViewById(R.id.jaw_description);
        TextView mTongue = (TextView) rootView.findViewById(R.id.tongue_description);
        Button mBtnHearPronunciation = (Button) rootView.findViewById(R.id.btnHearSound);

        // Assign value to parameters
        mMouthImage.setImageResource(this.getArguments().getInt("mouthPosition"));
        mLips.setText(this.getArguments().getString("lips"));
        mJaw.setText(this.getArguments().getString("jaw"));
        mTongue.setText(this.getArguments().getString("tongue"));

        // Creates a new text to speech object
        mTextToSpeech = new TextToSpeech(getContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS){
                    mResult = mTextToSpeech.setLanguage(Locale.US);
                } else {
                    // Feature not supported in your device.
                    Toast.makeText(getContext(), R.string.feature_not_supported, Toast.LENGTH_SHORT).show();
                }
            }
        });

        mBtnHearPronunciation.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mTextToSpeech != null){
            mTextToSpeech.stop();
            mTextToSpeech.shutdown();
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnHearSound) {
            if (mResult == TextToSpeech.LANG_NOT_SUPPORTED || mResult == TextToSpeech.LANG_MISSING_DATA){
                Toast.makeText(getContext(), R.string.feature_not_supported, Toast.LENGTH_SHORT).show();
            } else {
                mTextToSpeech.setPitch(1.8f);
                mTextToSpeech.setLanguage(Locale.US);
                String text = this.getArguments().getString("pronunciation");
                mTextToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null, null);
            }
        }
    }
}

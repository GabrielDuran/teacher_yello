package com.gabri.teacheryelloapp;

class User {

    // Attributes
    private String mId, mName, mEmail, mPassword;
    private int mLives;

    // Empty Constructor Method.
    User(){ super(); }

    // Constructor Method with Parameters.
    User (String id, String name, String email, String password, int lives){
        this.mId = id;
        this.mName = name;
        this.mEmail = email;
        this.mPassword = password;
        this.mLives = lives;
    }

    // Get string ID.
    public String getId(){
        return mId;
    }

    // Set string ID
    public void setId(String id) {
        this.mId = id;
    }

    // Get string Name
    public String getName() {
        return mName;
    }

    // Set string Name
    public void setName(String name) {
        this.mName = name;
    }

    // Get string Email
    public String getEmail() {
        return mEmail;
    }

    // Set string Email
    void setEmail(String email) {
        this.mEmail = email;
    }

    // Get string Password
    String getPassword() {
        return mPassword;
    }

    // Set string Password
    void setPassword(String password) {
        this.mPassword = password;
    }

    // Get int Lives
    int getLives() {
        return mLives;
    }

    // Set int Lives
    void setLives(int lives) {
        this.mLives = lives;
    }
}

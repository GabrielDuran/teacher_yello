package com.gabri.teacheryelloapp;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Locale;
import static android.app.Activity.RESULT_OK;
import static com.facebook.FacebookSdk.getApplicationContext;

public class FragmentPractice extends Fragment implements ISpeechRecognizer {

    //Attributes
    private SentencesAdapter mAdapter;
    Sentences[] sentencesData;
    int currentPosition = -1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_practice, container, false);

        ListView iPracticeList = (ListView) rootView.findViewById(R.id.practice_list);

        sentencesData = (Sentences[]) getArguments().getSerializable("practiceList");

        mAdapter = new SentencesAdapter(getContext(), R.layout.listview_item_row, sentencesData, this);
        iPracticeList.setAdapter(mAdapter);

        return rootView;
    }

    public void promptSpeechInput(String title, int position){
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.ENGLISH);
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, title);
        currentPosition = position;

        try {
            startActivityForResult(intent,100);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(getApplicationContext(),R.string.speech_not_supported,Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        switch (requestCode) {
            case 100: if (resultCode == RESULT_OK && intent != null) {
                ArrayList<String> result = intent.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                sentencesData[currentPosition].setResult(result.get(0).substring(0,1).toUpperCase() + result.get(0).substring(1));
                mAdapter.notifyDataSetChanged();
            }
                break;
        }
    }
}

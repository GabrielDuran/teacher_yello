package com.gabri.teacheryelloapp;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

// Class Sentences Adapter
class SentencesAdapter extends ArrayAdapter<Sentences>{
    private Context mContext;
    private int mLayoutResourceID;
    private Sentences mData[] = null;
    private ISpeechRecognizer mRecognizer;

    // Gets list elements
    SentencesAdapter(Context context, int layoutResourceID, Sentences[] data, ISpeechRecognizer recognizer){
        super(context, layoutResourceID, data);
        this.mContext = context;
        this.mLayoutResourceID = layoutResourceID;
        this.mData = data;
        this.mRecognizer = recognizer;
    }

    // Inflates list item
    @NonNull
    public View getView(final int position, View convertView, @NonNull ViewGroup parent){
        View row = convertView;
        final SentencesHolder holder;
        final Sentences  sentences = mData[position];
        if (row == null){
            LayoutInflater inflater = ((Activity)mContext).getLayoutInflater();
            row = inflater.inflate(mLayoutResourceID, parent, false);
            holder = new SentencesHolder();
            holder.button = (ImageView) row.findViewById(R.id.image_mic);
            holder.sentence = (TextView) row.findViewById(R.id.sentence);
            holder.result = (TextView) row.findViewById(R.id.result);
            holder.button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mRecognizer.promptSpeechInput(sentences.sentence, position);
                }
            });
            row.setTag(holder);
        } else {
            holder = (SentencesHolder) row.getTag();
        }
        holder.button.setImageResource(sentences.button);
        holder.sentence.setText(sentences.sentence);
        holder.result.setText(sentences.result);
        return row;
    }

    // Holds elements list
    private static class SentencesHolder{
        ImageView button;
        TextView sentence;
        TextView result;
    }
}

package com.gabri.teacheryelloapp;

import android.app.Application;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

// Class Teacher Yello
public class TeacherYelloApp extends Application{

    // On create activity
    @Override
    public void onCreate() {
        super.onCreate();

        // Initialize facebook sdk
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
    }
}

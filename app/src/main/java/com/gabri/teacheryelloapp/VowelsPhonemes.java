package com.gabri.teacheryelloapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

// Class Vowels Phonemes
public class VowelsPhonemes extends AppCompatActivity {

    Sentences[] sentencesData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vowels_phonemes);
    }

    // Send values to practice with [i]
    public void practiceLongI(View view) {
        // Parameters
        int mouthPosition = R.drawable.mouth_long_i;
        String lips = "Tense and in a 'smile' position";
        String jaw = "Almost completely raised";
        String tongue = "High, near the roof of the mouth";
        String pronunciationValue = "<speak xml:lang=\"en-US\"> <phoneme alphabet=\"ipa\" ph=\"i:\"/>.</speak>";

        sentencesData = new Sentences[]{
                new Sentences(R.drawable.microphone, "See you next week", ""),
                new Sentences(R.drawable.microphone, "You are in my team", ""),
                new Sentences(R.drawable.microphone, "Call the police", ""),
                new Sentences(R.drawable.microphone, "Pleased to meet you", ""),
                new Sentences(R.drawable.microphone, "Steve eats cream cheese", ""),
        };

        Intent intent = new Intent(this, PracticeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

        // Set parameters to activity
        intent.putExtra("activityTitle", "Pronouncing [ i ]");
        intent.putExtra("mouthPosition", mouthPosition);
        intent.putExtra("lips", lips);
        intent.putExtra("jaw", jaw);
        intent.putExtra("tongue", tongue);
        intent.putExtra("pronunciation", pronunciationValue);
        intent.putExtra("practiceList", sentencesData);

        // Start activity
        startActivity(intent);
        finish();
    }

    // Send values to practice with [I]
    public void practiceShortI(View view) {
        // Parameters
        int mouthPosition = R.drawable.mouth_short_i;
        String lips = "Relaxed and slightly parted";
        String jaw = "Slightly lower than for [ i ]";
        String tongue = "High, but lower than for [ i ]";
        String pronunciationValue = "<speak xml:lang=\"en-US\"> <phoneme alphabet=\"ipa\" ph=\"ɪ\"/>.</speak>";

        sentencesData = new Sentences[]{
                new Sentences(R.drawable.microphone, "This is it", ""),
                new Sentences(R.drawable.microphone, "This is Miss Smith", ""),
                new Sentences(R.drawable.microphone, "I will sit in a minute", ""),
                new Sentences(R.drawable.microphone, "This is my sister", ""),
                new Sentences(R.drawable.microphone, "Did you give him his gift?", ""),
        };

        Intent intent = new Intent(this, PracticeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

        // Set parameters to activity
        intent.putExtra("activityTitle", "Pronouncing [ I ]");
        intent.putExtra("mouthPosition", mouthPosition);
        intent.putExtra("lips", lips);
        intent.putExtra("jaw", jaw);
        intent.putExtra("tongue", tongue);
        intent.putExtra("pronunciation", pronunciationValue);
        intent.putExtra("practiceList", sentencesData);

        // Start activity
        startActivity(intent);
        finish();
    }

    // Send values to practice with [ɛ]
    public void practiceE(View view) {
        // Parameters
        int mouthPosition = R.drawable.mouth_e;
        String lips = "Slightly spread and unrounded";
        String jaw = "Open wider than for [ I ]";
        String tongue = "High, near the roof of the mouth";
        String pronunciationValue = "<speak xml:lang=\"en-US\"> <phoneme alphabet=\"ipa\" ph=\"ɛ\"/>.</speak>";

        sentencesData = new Sentences[]{
                new Sentences(R.drawable.microphone, "Best friend", ""),
                new Sentences(R.drawable.microphone, "Never better", ""),
                new Sentences(R.drawable.microphone, "Head of lettuce", ""),
                new Sentences(R.drawable.microphone, "Fred left a message", ""),
                new Sentences(R.drawable.microphone, "Let me get some rest", ""),
        };

        Intent intent = new Intent(this, PracticeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

        // Set parameters to activity
        intent.putExtra("activityTitle", "Pronouncing [ ɛ ]");
        intent.putExtra("mouthPosition", mouthPosition);
        intent.putExtra("lips", lips);
        intent.putExtra("jaw", jaw);
        intent.putExtra("tongue", tongue);
        intent.putExtra("pronunciation", pronunciationValue);
        intent.putExtra("practiceList", sentencesData);

        // Start activity
        startActivity(intent);
        finish();
    }

    // Send values to practice with [æ]
    public void practiceAE(View view) {
        // Parameters
        int mouthPosition = R.drawable.mouth_ae;
        String lips = "Spread";
        String jaw = "Open wider than for [ ɛ ]";
        String tongue = "Low, near the floor of the mouth";
        String pronunciationValue = "<speak xml:lang=\"en-US\"> <phoneme alphabet=\"ipa\" ph=\"æ\"/>.</speak>";

        sentencesData = new Sentences[]{
                new Sentences(R.drawable.microphone, "Last chance", ""),
                new Sentences(R.drawable.microphone, "At a glance", ""),
                new Sentences(R.drawable.microphone, "Wrap it up", ""),
                new Sentences(R.drawable.microphone, "Is that a fact?", ""),
                new Sentences(R.drawable.microphone, "I have to catch it", ""),
        };

        Intent intent = new Intent(this, PracticeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

        // Set parameters to activity
        intent.putExtra("activityTitle", "Pronouncing [ æ ]");
        intent.putExtra("mouthPosition", mouthPosition);
        intent.putExtra("lips", lips);
        intent.putExtra("jaw", jaw);
        intent.putExtra("tongue", tongue);
        intent.putExtra("pronunciation", pronunciationValue);
        intent.putExtra("practiceList", sentencesData);

        // Start activity
        startActivity(intent);
        finish();
    }

    // Send values to practice with [a]
    public void practiceA(View view) {
        // Parameters
        int mouthPosition = R.drawable.mouth_a;
        String lips = "Completely apart in a 'yawning' position";
        String jaw = "Lower than for any other vowel";
        String tongue = "Flat, on the floor of the mouth";
        String pronunciationValue = "<speak xml:lang=\"en-US\"> <phoneme alphabet=\"ipa\" ph=\"ɑː\"/>.</speak>";

        sentencesData = new Sentences[]{
                new Sentences(R.drawable.microphone, "Alarm clock", ""),
                new Sentences(R.drawable.microphone, "Stock market", ""),
                new Sentences(R.drawable.microphone, "Not far apart", ""),
                new Sentences(R.drawable.microphone, "Top to bottom", ""),
                new Sentences(R.drawable.microphone, "My father park the car", ""),
        };

        Intent intent = new Intent(this, PracticeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

        // Set parameters to activity
        intent.putExtra("activityTitle", "Pronouncing [ a ]");
        intent.putExtra("mouthPosition", mouthPosition);
        intent.putExtra("lips", lips);
        intent.putExtra("jaw", jaw);
        intent.putExtra("tongue", tongue);
        intent.putExtra("pronunciation", pronunciationValue);
        intent.putExtra("practiceList", sentencesData);

        // Start activity
        startActivity(intent);
        finish();
    }

    // Send values to practice with [u]
    public void practiceLongU(View view) {
        // Parameters
        int mouthPosition = R.drawable.mouth_long_u;
        String lips = "Tense and in a 'whistling' position";
        String jaw = "Almost completely raised";
        String tongue = "High, near the roof of the mouth";
        String pronunciationValue = "<speak xml:lang=\"en-US\"> <phoneme alphabet=\"ipa\" ph=\"uː\"/>.</speak>";

        sentencesData = new Sentences[]{
                new Sentences(R.drawable.microphone, "Who are you?", ""),
                new Sentences(R.drawable.microphone, "Loose tooth", ""),
                new Sentences(R.drawable.microphone, "In the mood", ""),
                new Sentences(R.drawable.microphone, "School news", ""),
                new Sentences(R.drawable.microphone, "You lose the shoe ", ""),
        };

        Intent intent = new Intent(this, PracticeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

        // Set parameters to activity
        intent.putExtra("activityTitle", "Pronouncing [ u ]");
        intent.putExtra("mouthPosition", mouthPosition);
        intent.putExtra("lips", lips);
        intent.putExtra("jaw", jaw);
        intent.putExtra("tongue", tongue);
        intent.putExtra("pronunciation", pronunciationValue);
        intent.putExtra("practiceList", sentencesData);

        // Start activity
        startActivity(intent);
        finish();
    }

    // Send values to practice with [U]
    public void practiceShortU(View view) {
        // Parameters
        int mouthPosition = R.drawable.mouth_short_u;
        String lips = "Relaxed and slightly parted";
        String jaw = "Slightly lower than for [ u ]";
        String tongue = "High, but lower than for [ u ]";
        String pronunciationValue = "<speak xml:lang=\"en-US\"> <phoneme alphabet=\"ipa\" ph=\"ʊ\"/>.</speak>";

        sentencesData = new Sentences[]{
                new Sentences(R.drawable.microphone, "Take a good look", ""),
                new Sentences(R.drawable.microphone, "Should we go", ""),
                new Sentences(R.drawable.microphone, "Who took my book", ""),
                new Sentences(R.drawable.microphone, "Put the wood away", ""),
                new Sentences(R.drawable.microphone, "The woman eats sugar", ""),
        };

        Intent intent = new Intent(this, PracticeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

        // Set parameters to activity
        intent.putExtra("activityTitle", "Pronouncing [ U ]");
        intent.putExtra("mouthPosition", mouthPosition);
        intent.putExtra("lips", lips);
        intent.putExtra("jaw", jaw);
        intent.putExtra("tongue", tongue);
        intent.putExtra("pronunciation", pronunciationValue);
        intent.putExtra("practiceList", sentencesData);

        // Start activity
        startActivity(intent);
        finish();
    }

    // Send values to practice with [ʌ]
    public void practiceShortA(View view) {
        // Parameters
        int mouthPosition = R.drawable.mouth_hut;
        String lips = "Relaxed and slightly parted";
        String jaw = "Relaxed and slightly lowered";
        String tongue = "Relaxed and midlevel in the mouth";
        String pronunciationValue = "<speak xml:lang=\"en-US\"> <phoneme alphabet=\"ipa\" ph=\"ʌ\"/>.</speak>";

        sentencesData = new Sentences[]{
                new Sentences(R.drawable.microphone, "Come in", ""),
                new Sentences(R.drawable.microphone, "Bubble gum", ""),
                new Sentences(R.drawable.microphone, "Once is enough", ""),
                new Sentences(R.drawable.microphone, "Cover up", ""),
                new Sentences(R.drawable.microphone, "What does it means", ""),
        };

        Intent intent = new Intent(this, PracticeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

        // Set parameters to activity
        intent.putExtra("activityTitle", "Pronouncing [ ʌ ]");
        intent.putExtra("mouthPosition", mouthPosition);
        intent.putExtra("lips", lips);
        intent.putExtra("jaw", jaw);
        intent.putExtra("tongue", tongue);
        intent.putExtra("pronunciation", pronunciationValue);
        intent.putExtra("practiceList", sentencesData);

        // Start activity
        startActivity(intent);
        finish();
    }

    // Send values to practice with [ɔ]
    public void practiceO(View view) {
        // Parameters
        int mouthPosition = R.drawable.mouth_o;
        String lips = "In a tense oval shape and slightly protruded";
        String jaw = "Open more than for [ U ]";
        String tongue = "Low, near the floor of the mouth";
        String pronunciationValue = "<speak xml:lang=\"en-US\"> <phoneme alphabet=\"ipa\" ph=\"ɔː\"/>.</speak>";

        sentencesData = new Sentences[]{
                new Sentences(R.drawable.microphone, "Call it off", ""),
                new Sentences(R.drawable.microphone, "Call the shots", ""),
                new Sentences(R.drawable.microphone, "All talk", ""),
                new Sentences(R.drawable.microphone, "Walk all over", ""),
                new Sentences(R.drawable.microphone, "It is all wrong", ""),
        };

        Intent intent = new Intent(this, PracticeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

        // Set parameters to activity
        intent.putExtra("activityTitle", "Pronouncing [ ɔ ]");
        intent.putExtra("mouthPosition", mouthPosition);
        intent.putExtra("lips", lips);
        intent.putExtra("jaw", jaw);
        intent.putExtra("tongue", tongue);
        intent.putExtra("pronunciation", pronunciationValue);
        intent.putExtra("practiceList", sentencesData);

        // Start activity
        startActivity(intent);
        finish();
    }

    // Send values to practice with [ə]
    public void practiceInverseE(View view) {
        // Parameters
        int mouthPosition = R.drawable.mouth_e_inverse;
        String lips = "Completely relaxed and barely move during its pronunciation.";
        String jaw = "Slightly lower than for [ ʌ ]";
        String tongue = "High, but lower than for [ ʌ ]";
        String pronunciationValue = "<speak xml:lang=\"en-US\"> <phoneme alphabet=\"ipa\" ph=\"ə\"/>.</speak>";

        // List data
        sentencesData = new Sentences[]{
                new Sentences(R.drawable.microphone, "Don't complain", ""),
                new Sentences(R.drawable.microphone, "I suppose so", ""),
                new Sentences(R.drawable.microphone, "See you tomorrow", ""),
                new Sentences(R.drawable.microphone, "Are you alone?", ""),
                new Sentences(R.drawable.microphone, "Once upon a time", ""),
        };

        Intent intent = new Intent(this, PracticeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

        // Set parameters to activity
        intent.putExtra("activityTitle", "Pronouncing [ ə ]");
        intent.putExtra("mouthPosition", mouthPosition);
        intent.putExtra("lips", lips);
        intent.putExtra("jaw", jaw);
        intent.putExtra("tongue", tongue);
        intent.putExtra("pronunciation", pronunciationValue);
        intent.putExtra("practiceList", sentencesData);

        // Start activity
        startActivity(intent);
        finish();
    }
}

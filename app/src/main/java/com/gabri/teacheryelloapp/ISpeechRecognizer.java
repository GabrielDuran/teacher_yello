package com.gabri.teacheryelloapp;

// Speech Recognizer Interface
interface ISpeechRecognizer {
    // Implements prompt speech result
    void promptSpeechInput(String title, int position);
}

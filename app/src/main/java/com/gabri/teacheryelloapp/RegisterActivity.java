package com.gabri.teacheryelloapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

// Class Register Activity
public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    // Attributes
    private Button mBtnSignUp;
    private EditText mEmail, mName, mPassword, mConfirmPassword;
    private ProgressDialog mProgressDialog;
    private FirebaseAuth mFirebaseAuth;
    private DatabaseReference mDatabaseReference;

    // On Create Activity
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        // Match variables with U/I id elements
        mName = (EditText) findViewById(R.id.etName);
        mEmail = (EditText) findViewById(R.id.etEmail);
        mPassword = (EditText) findViewById(R.id.etPassword);
        mConfirmPassword = (EditText) findViewById(R.id.etConfirmPassword);
        mBtnSignUp = (Button) findViewById(R.id.btnSignUp);

        // ActionBar
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Create a new progress bar
        mProgressDialog = new ProgressDialog(this);

        // Gets firebase auth instance
        mFirebaseAuth = FirebaseAuth.getInstance();

        // Creates a new reference in database
        mDatabaseReference = FirebaseDatabase.getInstance().getReference().child("Users");

        // Set on click method to sign up button
        mBtnSignUp.setOnClickListener(this);
    }

    // Register user into database
    private void registerUser (){
        // Get text from edit text
        final String name = mName.getText().toString().trim();
        final String email = mEmail.getText().toString().trim();
        String password = mPassword.getText().toString().trim();
        String confirm_password = mConfirmPassword.getText().toString().trim();

        // Create an email pattern
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        // Validate that not must exist empty fields
        if (TextUtils.isEmpty(email) || TextUtils.isEmpty(name) || TextUtils.isEmpty(password) || TextUtils.isEmpty(confirm_password)) {
            Toast.makeText(this, R.string.data_require, Toast.LENGTH_SHORT).show();
        }
        // Validate password and confirm password must be equals
        else if (!password.equals(confirm_password)) {
            Toast.makeText(this, R.string.not_match, Toast.LENGTH_SHORT).show();
        }
        // Validate email address pattern
        else if (!email.matches(emailPattern)){
            Toast.makeText(this, R.string.invalid_email_address, Toast.LENGTH_SHORT).show();
        }
        // Register the user
        else {
            // Show progress dialog
            mProgressDialog.setMessage("Signing Up...");
            mProgressDialog.show();

            // Encrypts password text
            final String encrypt_password = md5(password);

            // Sets up the new user
            setUpUser();

            // Creates a email and password authentication
            mFirebaseAuth.createUserWithEmailAndPassword(email, encrypt_password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {

                                //User user = new User();
                                String user_id = task.getResult().getUser().getUid();
                                DatabaseReference current_user = mDatabaseReference.child("user_id: "+user_id);
                                current_user.child("name: ").setValue(name);
                                current_user.child("email: ").setValue(email);
                                current_user.child("password: ").setValue(encrypt_password);
                                current_user.child("image: ").setValue("default");
                                current_user.child("lives: ").setValue(3);

                                // Start Login Activity
                                FirebaseAuth.getInstance().signOut();
                                goLoginActivity();

                                // Close progress dialog
                                mProgressDialog.dismiss();

                                // User registered successfully
                                Toast.makeText(RegisterActivity.this, R.string.registered_successfully, Toast.LENGTH_SHORT).show();
                            } else {
                                // Validates if exist internet connection
                                if (isNetworkAvailable()){
                                    // Close progress dialog
                                    mProgressDialog.dismiss();
                                    Toast.makeText(RegisterActivity.this, R.string.register_faild, Toast.LENGTH_LONG).show();
                                } else {
                                    // Close progress dialog
                                    mProgressDialog.dismiss();
                                    Toast.makeText(RegisterActivity.this, R.string.internet_connection_error, Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    });
        }
    }

    // On activity stop
    @Override
    protected void onStop() {
        super.onStop();
    }

    // On back button pressed
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        // Goes to login activity
        goLoginActivity();
    }

    // On button sign up is clicked
    @Override
    public void onClick(View v) {
        if (v == mBtnSignUp) {
            // Register new user
            registerUser();
        }
    }

    // Sets up a new user by fetching the user entered details
    protected void setUpUser() {
        User user = new User();
        user.setName(mName.getText().toString());
        user.setEmail(mEmail.getText().toString());
        user.setPassword(md5(mPassword.getText().toString()));
    }

    // Goes login activity
    public void goLoginActivity(){
        Intent loginIntent = new Intent(this, LoginActivity.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(loginIntent);
        finish();
    }

    // Encrypt password
    private static String md5 (final String password) {
        try {
            MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
            digest.update(password.getBytes());
            byte messageDigest[] = digest.digest();
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest){
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();
        } catch (NoSuchAlgorithmException e){
            e.printStackTrace();
        }
        return "";
    }

    // Check if exist internet connection
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(RegisterActivity.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
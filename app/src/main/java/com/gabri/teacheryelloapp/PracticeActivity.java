package com.gabri.teacheryelloapp;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import java.io.Serializable;
import java.util.ArrayList;

public class PracticeActivity extends AppCompatActivity {

    // Attributes
    private int mMouthPosition;
    private String mLips, mJaw, mTongue, mPronunciation;
    private Sentences[] mList;

    // On create activity
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_practices);

        // Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getIntent().getStringExtra("activityTitle"));
        setSupportActionBar(toolbar);

        mMouthPosition = getIntent().getIntExtra("mouthPosition", 0);
        mLips = getIntent().getStringExtra("lips");
        mJaw = getIntent().getStringExtra("jaw");
        mTongue = getIntent().getStringExtra("tongue");
        mPronunciation = getIntent().getStringExtra("pronunciation");
        mList = (Sentences[]) getIntent().getSerializableExtra("practiceList");

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        SectionsPagerAdapter mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        ViewPager mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_practices, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_leave_exercise) {
            // Goes to main activity
            goVowelsActivity();
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        SectionsPagerAdapter(FragmentManager fm) { super(fm); }

        // Select the fragment
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    Bundle bundle = new Bundle();
                    bundle.putInt("mouthPosition", mMouthPosition);
                    bundle.putString("lips", mLips);
                    bundle.putString("jaw", mJaw);
                    bundle.putString("tongue", mTongue);
                    bundle.putString("pronunciation", mPronunciation);
                    FragmentExplanation fragmentExplanation = new FragmentExplanation();
                    fragmentExplanation.setArguments(bundle);
                    return fragmentExplanation;
                case 1:
                    Bundle listBundle = new Bundle();
                    listBundle.putSerializable("practiceList", mList);
                    FragmentPractice fragmentPractice = new FragmentPractice();
                    fragmentPractice.setArguments(listBundle);
                    return fragmentPractice;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            // Show 2 total pages.
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "EXPLANATION";
                case 1:
                    return "PRACTICE";
            }
            return null;
        }
    }

    // Goes to main activity
    public void goVowelsActivity(){
        Intent vowelsIntent = new Intent(this, VowelsPhonemes.class);
        vowelsIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(vowelsIntent);
        finish();
    }
}
